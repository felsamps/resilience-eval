import numpy as np

from Definitions import *

class Video:
    def __init__(self, video, resolution, numberOfFrames=9):
        self.__width = resolution[0]
        self.__height = resolution[1]
        self.__numberOfFrames = numberOfFrames

        self.__videoFilePath = videosPath + video
        self.__fp = open(self.__videoFilePath, 'rb')

        self.__readVideoData()

    def __readVideoData(self):
        self.__data = []

        for f in range(0, self.__numberOfFrames):
            frame = []

            for h in range(0, self.__height):
                row = bytearray(self.__fp.read(self.__width))
                frame.append(row)

            self.__data.append(frame)

            #discard chroma samples
            self.__fp.read(int(self.__width * self.__height * 0.5))

        #initialize numpy array
        self.__npData = np.array(self.__data)

    def saveFrameIntoFile(self, fileName, frameId):
        fpTemp = open(reportPath + fileName, 'w')

        for h in range(0, self.__height):
            for w in range(0, self.__width):
                fpTemp.write(str(self.__npData[frameId, h, w]) + ' ')
            fpTemp.write('\n')

        fpTemp.close()


    def getFrameBlock(self, frameId, xBlock, yBlock, size):
        return np.transpose(self.__npData[frameId, yBlock:(yBlock+size) , xBlock:(xBlock+size)])

    def getWidth(self):
        return self.__width

    def getHeight(self):
        return self.__height

    def getNumOfFrames(self):
        return self.__numberOfFrames
