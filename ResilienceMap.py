import numpy as np
from Video import *

class ResilienceMap:
    def __init__(self, video, blockSize):
        self.__video = video
        self.__blockSize = blockSize
        self.__mapWidth = int(self.__video.getWidth() / self.__blockSize)
        self.__mapHeight = int(self.__video.getHeight() / self.__blockSize)

        self.__generate()

    def __generate(self):

        self.__map = np.empty(shape=(self.__video.getNumOfFrames(), self.__mapHeight, self.__mapWidth))

        for f in range(0, self.__video.getNumOfFrames()):
            for y in range(0, self.__mapHeight):
                for x in range(0, self.__mapWidth):
                    xPos = x * self.__blockSize
                    yPos = y * self.__blockSize

                    block = self.__video.getFrameBlock(f, xPos, yPos, self.__blockSize)
                    self.__map[f, y, x] = np.var(block)

    def saveFrameMapToFile(self, fileName, frameId):
        fpTemp = open(reportPath + fileName, 'w')

        for y in range(0, self.__mapHeight):
            for x in range(0, self.__mapWidth):
                fpTemp.write(str(self.__map[frameId, y, x]) + ' ')
            fpTemp.write('\n')

        fpTemp.close()




