from Video import *
from ResilienceMap import *

"""
Definitions for the experiments executions
"""

videos = {
#    'BQTerrace_1920x1080_60.yuv' : (1920, 1080),
   'BasketballDrive_1920x1080_50.yuv' : (1920, 1080),
#    'Cactus_1920x1080_50.yuv' : (1920, 1080),
#    'Kimono1_1920x1080_24.yuv' : (1920, 1080),
#    'BQSquare_416x240_60.yuv' : (416, 240),

    #include others
}

numberOfFrames = 9
blockGranularity = 8 # think more about it

if __name__ == '__main__':

    for video in videos.keys():

        # load YUV video from the file
        sequence = Video(video, videos[video])
        sequence.saveFrameIntoFile('frame.mat',0)

        resilienceMaps = ResilienceMap(sequence, 8)
        resilienceMaps.saveFrameMapToFile('varmap.mat', 0)

